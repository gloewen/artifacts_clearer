/*
 * Copyright (c) 2022 by ARM GmbH, www.ablex.com
 * ArtifactsClearer.java
 * created on 25.05.22, 16:36
 */

package com.arm.artifactsClearer;

import com.arm.artifactsClearer.dao.PackageRestDao;
import com.arm.artifactsClearer.dto.PackageDto;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * javaDoc
 *
 * @author gloewen
 */
public class ArtifactsClearer {

    private Predicate<PackageDto> deletePredicate;

    private PackageRestDao dao;

    public ArtifactsClearer(String token, int projectId, Predicate<PackageDto> deletePredicate) {
        this.deletePredicate = deletePredicate;

        this.dao = new PackageRestDao(token, projectId);
    }

    public List<PackageDto> findPackages() {
        return dao.findAllPackages().stream().filter(deletePredicate).collect(Collectors.toList());
    }

    public List<PackageDto> deleteFoundPackages(List<PackageDto> packages) {
        packages.forEach(packageDto -> dao.deletePackage(packageDto));

        return packages;
    }
}
