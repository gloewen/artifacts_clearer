/*
 * Copyright (c) 2022 by ARM GmbH, www.ablex.com
 * PackageDto.java
 * created on 25.05.22, 16:36
 */

package com.arm.artifactsClearer.dto;

import java.util.List;

/**
 * javaDoc
 *
 * @author gloewen
 */
public class PackageDto {

    private int id;
    private String name;
    private String version;
    private String package_type;
    private String status;
    private LinksDto _links;
    private String created_at;
    private List<String> tags;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public String getPackageType() {
        return package_type;
    }

    public String getStatus() {
        return status;
    }

    public LinksDto getLinks() {
        return _links;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public List<String> getTags() {
        return tags;
    }
}
