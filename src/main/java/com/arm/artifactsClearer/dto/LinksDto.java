/*
 * Copyright (c) 2022 by ARM GmbH, www.ablex.com
 * LinksDto.java
 * created on 25.05.22, 16:36
 */

package com.arm.artifactsClearer.dto;

/**
 * javaDoc
 *
 * @author gloewen
 */
public class LinksDto {

    private String web_path;
    private String delete_api_path;

    public String getWebPath() {
        return web_path;
    }

    public String getDeleteApiPath() {
        return delete_api_path;
    }
}
