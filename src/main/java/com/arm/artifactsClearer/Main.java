/*
 * Copyright (c) 2022 by ARM GmbH, www.ablex.com
 * Main.java
 * created on 25.05.22, 16:36
 */

package com.arm.artifactsClearer;

import com.arm.artifactsClearer.dto.PackageDto;
import com.arm.artifactsClearer.predicate.AgePredicate;
import com.arm.artifactsClearer.predicate.VersionNamePredicate;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;

/**
 * javaDoc
 *
 * @author gloewen
 */
public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("**************************************************");
        System.out.println("GitLab Artifacts Clearer");
        System.out.println("--------------------------------------------------");
        System.out.println();

        System.out.println("Please enter project-id: ");
        int projectId = scanner.nextInt();

        System.out.println("Please enter token: ");
        String token = scanner.next();

        Predicate<PackageDto> deletePredicate = packageDto -> true;

        System.out.println("Filter by age? [yes/no]");
        String filterByAgeAnswer = scanner.next();
        if (filterByAgeAnswer.equalsIgnoreCase("yes")) {
            System.out.println("Only find packages created before (e.g. '2022-05-25'): ");
            deletePredicate = deletePredicate.and(new AgePredicate(LocalDate.parse(scanner.next())));
        }

        System.out.println("Filter by version name? [yes/no]");
        String filterByVersionNameAnswer = scanner.next();
        if (filterByVersionNameAnswer.equalsIgnoreCase("yes")) {
            System.out.println("Only find packages with a version that contains the following string (not case sensitive; e.g. 'snap' or '1.0'): ");
            deletePredicate = deletePredicate.and(new VersionNamePredicate(scanner.next()));
        }

        System.out.println();
        System.out.println("--------------------------------------------------");
        System.out.println("...loading...");
        System.out.println();

        ArtifactsClearer artifactsClearer = new ArtifactsClearer(token, projectId, deletePredicate);
        List<PackageDto> foundPackages = artifactsClearer.findPackages();

        printPackages(foundPackages);

        System.out.println();
        System.out.println();

        System.out.println("Delete all matching packages? [yes/no]");
        String deleteAnswer = scanner.next();
        if (deleteAnswer.equalsIgnoreCase("yes")) {
            System.out.println("--------------------------------------------------");
            System.out.println("...deleting...");
            System.out.println();
            List<PackageDto> deletedPackages = artifactsClearer.deleteFoundPackages(foundPackages);
            System.out.println("\n" + deletedPackages.size() + " packages deleted");
        }
        System.out.println();
        System.out.println("**************************************************");
    }

    private static void printPackages(List<PackageDto> foundPackages) {
        foundPackages.forEach(packageDto -> System.out.println(packageDto.getVersion() + "\t" + packageDto.getLinks().getDeleteApiPath()));
        System.out.println("\n" + foundPackages.size() + " matching packages found");
    }
}
