/*
 * Copyright (c) 2022 by ARM GmbH, www.ablex.com
 * PackageRestDao.java
 * created on 25.05.22, 16:36
 */

package com.arm.artifactsClearer.dao;

import com.arm.artifactsClearer.dto.PackageDto;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * javaDoc
 *
 * @author gloewen
 */
public class PackageRestDao {

    private Gson gson = new GsonBuilder().create();

    private String token;

    private int projectId;

    public PackageRestDao(String token, int projectId) {
        this.token = token;
        this.projectId = projectId;
    }

    public List<PackageDto> findAllPackages() {
        List<PackageDto> packages = new ArrayList<>();

        try {
            int page = 1;
            Response response = executeGet(page++);
            packages.addAll(Arrays.asList(gson.fromJson((response.body().string()), PackageDto[].class)));

            while (Integer.parseInt(response.header("x-page")) < Integer.parseInt(response.header("x-total-pages"))) {
                response = executeGet(page++);
                packages.addAll(Arrays.asList(gson.fromJson((response.body().string()), PackageDto[].class)));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return packages;
    }

    public void deletePackage(PackageDto packageDto) {
        System.out.println("delete " + packageDto.getVersion() + "\t" + packageDto.getLinks().getDeleteApiPath());
        executeDelete(packageDto.getLinks().getDeleteApiPath());
    }

    private Response executeGet(int page) throws MalformedURLException {
        return executeRequest(buildRequestBuilder()
                .url(new URL("https://gitlab.com/api/v4/projects/" + projectId + "/packages?per_page=100&page=" + page))
                .get()
                .build());
    }

    private Response executeDelete(String deleteUrl) {
        return executeRequest(buildRequestBuilder()
                .url(deleteUrl)
                .delete()
                .build());
    }

    private Request.Builder buildRequestBuilder() {
        return new Request.Builder()
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Authorization", "Bearer " + token);
    }

    private Response executeRequest(Request request) {
        try {
            OkHttpClient client = new OkHttpClient().newBuilder().build();
            Response response = client.newCall(request).execute();

            int responseCode = response.code();
            if (responseCode < 300) {
                return response;
            } else {
                throw new RuntimeException("Http request returned error code: " + responseCode + "\n" + response.body().string());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
