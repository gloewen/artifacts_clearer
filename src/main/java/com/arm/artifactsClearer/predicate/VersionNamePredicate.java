/*
 * Copyright (c) 2022 by ARM GmbH, www.ablex.com
 * VersionNamePredicate.java
 * created on 25.05.22, 16:36
 */

package com.arm.artifactsClearer.predicate;

import com.arm.artifactsClearer.dto.PackageDto;

import java.util.function.Predicate;

/**
 * javaDoc
 *
 * @author gloewen
 */
public class VersionNamePredicate implements Predicate<PackageDto> {

    private String versionSubstring;

    public VersionNamePredicate(String versionSubstring) {
        this.versionSubstring = versionSubstring;
    }

    @Override
    public boolean test(PackageDto packageDto) {
        return packageDto.getVersion().toUpperCase().contains(versionSubstring.toUpperCase());
    }
}
