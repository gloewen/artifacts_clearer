/*
 * Copyright (c) 2022 by ARM GmbH, www.ablex.com
 * AgePredicate.java
 * created on 25.05.22, 16:36
 */

package com.arm.artifactsClearer.predicate;

import com.arm.artifactsClearer.dto.PackageDto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Predicate;

/**
 * javaDoc
 *
 * @author gloewen
 */
public class AgePredicate implements Predicate<PackageDto> {

    private LocalDate dateLimit;

    public AgePredicate(LocalDate dateLimit) {
        this.dateLimit = dateLimit;
    }

    @Override
    public boolean test(PackageDto packageDto) {
        return LocalDateTime.parse(packageDto.getCreatedAt().replace("Z", ""), DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                .isBefore(dateLimit.atStartOfDay());
    }
}
