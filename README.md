# GitLab Artifacts Clearer

This is a tool for clearing your GitLab package registry. Since one by one deleting is related to a considerable expense the artifacts clearer should provide an easy way to delete multiple filtered artifacts at once using the GitLab API.

### How to use
Therefore you only need to run this application
- enter your `project id` into the console
- enter your `access token` ([This is how to create a personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html))
- enter `yes` if you want to filter by the artifact create date
  - enter the max `create date` (*YYYY-MM-DD*)
- enter `yes` if you want to filter by the artifact version name
    - enter a `string` that should be contained by the version name (not case-sensitive). This is useful if you only want to delete SNAPSHOTS or artifacts of a specific version.

![](src/main/resources/screenshot_1.jpg)

Now the application will list all matching versions and ask you to delete them.

![](src/main/resources/screenshot_2.jpg)

### Extensions
You could also define your own predicate for filtering by modifying the code (`ArtifactsClearer.class` accepts `Predicate<PackageDto> deletePredicate` as argument).

It is possible to write some extra logic for keeping several latest versions per major version, too.


### Additional Information
**Notice:** 
As you can see in the code this application deletes only the listed artifacts when 'yes' was entered. Nevertheless I don't take any responsibility for mistakenly deleted data especially if the code was modified.

If you have any questions just let me know :)
